package main

import (
	"gitlab.com/fizzycfg/chips/airfrier/pkg/print"
	"gitlab.com/fizzycfg/chips/broiler/cmd/broiler"
)

func main() {
	if err := broiler.MainCmd.Execute(); err != nil {
		print.Fatal(err)
	}
}
