module gitlab.com/fizzycfg/chips/broiler

go 1.14

require (
	github.com/spf13/cobra v1.0.0
	github.com/spf13/viper v1.7.1
	gitlab.com/fizzycfg/chips/airfrier v0.1.2
)
