package broiler

import (
	"github.com/spf13/cobra"
)

var (
	spawnAllCmd = &cobra.Command{
		Use:   "all",
		Short: "Spawn all applications",
		RunE:  spawnAll,
	}
)

func init() {
	// Add subcommand
	MainCmd.AddCommand(spawnAllCmd)
}

func spawnAll(command *cobra.Command, args []string) error {
	return nil
}
