package broiler

import (
	"github.com/spf13/cobra"
	config "gitlab.com/fizzycfg/chips/airfrier/pkg/config"
)

var verbose bool

// MainCmd represents the base command when called without any subcommands
var MainCmd = &cobra.Command{
	Use:   "broiler",
	Short: "Manage startup applications",
	Long:  "Broiler (formerly known as `startup-setup`) is a simple tool to manage startup applications in a DE-less GNU/Linux system",
}

func init() {
	cobra.OnInitialize(func() { config.Init(MainCmd.Name()) })
	// Add generic flags
	MainCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "verbose output")
	// Bind configuration with flags
	config.AddFlags(MainCmd)
}
