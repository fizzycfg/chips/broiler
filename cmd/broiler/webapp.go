package broiler

import (
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/fizzycfg/chips/airfrier/pkg/exec"
	"gitlab.com/fizzycfg/chips/airfrier/pkg/print"
)

var (
	spawnWebAppCmd = &cobra.Command{
		Use:   "webapp",
		Short: "Spawn a Web Application in standalone mode",
		RunE:  spawnWebApp,
	}
)

func init() {
	// Add subcommand
	MainCmd.AddCommand(spawnWebAppCmd)
}

type WebAppConfig struct {
	name    string
	profile string
}

type WebAppsConfig struct {
	apps []WebAppConfig
}

func parseConfig() error {
	var webAppsConfig WebAppsConfig
	err := viper.Unmarshal(&webAppsConfig)
	if err != nil {
		print.Fatal("failed to load configuration: %v", err)
	}

	// Parse them

	print.Success("webapps configuration successfully parsed")
	return err
}

func spawnWebApp(command *cobra.Command, args []string) error {
	if err := exec.CheckProgram("chromium"); err != nil {
		return err
	}
	return nil
}
